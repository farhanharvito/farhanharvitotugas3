import { StyleSheet, Text, TouchableOpacity, View, Modal } from 'react-native'
import React, { useState } from 'react'
import RNDateTimePicker from '@react-native-community/datetimepicker'

export default function DateScreen() {
    const [date, showDate] = useState(false);
    const [time, showTime] = useState(false);
    
  return (

    <View style={styles.container}>
    <Modal visible={date} transparent={true} >
        <RNDateTimePicker value={new Date()} onChange={()=>showDate(false)} mode='date' />
    </Modal>
      <TouchableOpacity onPress={()=>showDate(true)} style={styles.btnPress}>
        <Text style={styles.text}>
            Show Date
        </Text>
      </TouchableOpacity>

    <Modal visible={time} transparent={true} >
        <RNDateTimePicker value={new Date()} onChange={()=>showTime(false)} mode='time' />
    </Modal>
      <TouchableOpacity onPress={()=>showTime(true)} style={styles.btnPress}>
        <Text style={styles.text}>
            Show time
        </Text>
      </TouchableOpacity>
    </View>
  )
}


const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text:{
        color: 'white',
        fontSize: 24,
    },
    btnPress:{
        width: 240,
        height: 48,
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: '#213555'
    }
})