import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';

const HomeScreen = ({navigation}) => {
  return (
    <View style={styles.root}>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('CarouselScreen')}>
        <Text style={styles.text}>Carousel</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('DateScreen')}>
        <Text style={styles.text}>Date Picker</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Slider')}>
        <Text style={styles.text}>Slider</Text>
      </TouchableOpacity>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#F0F0F0',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#213555',
    width: 240,
    height: 48,
    marginVertical: 14,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
  },
  text: {
    fontSize: 18,
    color: 'white',
  },
});
