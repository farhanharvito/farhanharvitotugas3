import { FlatList, StyleSheet, Text, View, Image, Dimensions } from 'react-native'
import React from 'react'

const CarouselScreen = () => {
    const screenWidth = Dimensions.get("window").width;
    const carouselData=[
        {
            id: "01",
            image: require("../../assets/img/img-1.jpg"),
        },
        {
            id: "02",
            image: require("../../assets/img/img-2.jpg"),
        },
        {
            id: "03",
            image: require("../../assets/img/img-3.png"),
        }
    ];

    const renderItem = ({item, index}) =>{
        return (
        <View>
            <Image source={item.image} style={{height: 200, width: screenWidth}} />
        </View>
        );
    }

  return (
    <View>
      <Text>CarouselScreen</Text>
      <FlatList data={carouselData} renderItem={renderItem} horizontal={true} />
    </View>
  )
}

export default CarouselScreen

const styles = StyleSheet.create({})