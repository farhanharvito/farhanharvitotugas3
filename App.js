import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

import HomeScreen from './src/screens/HomeScreen';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import CarouselScreen from './src/screens/CarouselScreen';
import DateScreen from './src/screens/DateScreen';
import SliderScreen from './src/screens/Slider';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="CarouselScreen" component={CarouselScreen} />
        <Stack.Screen name="DateScreen" component={DateScreen} />
        <Stack.Screen name="Slider" component={SliderScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App

const styles = StyleSheet.create({})